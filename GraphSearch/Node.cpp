#include "Node.h"

Node::Node(){
	this->marked = false;
}

void Node::putEdge(Edge *edge){
	edgeList.push_back(edge);
}

Edge *Node::getEdge(int edgeIndex){
	return edgeList.at(edgeIndex);
}

int Node::edgeListSize(){
	return edgeList.size();
}

void Node::setMarked(){
	this->marked = true;
}

void Node::setUnmarked(){
	this->marked = false;
}

bool Node::isMarked(){
	if (marked)
		return true;
	return false;
}

bool Node::isEdgeListEmpty(){
	if (edgeList.empty())
		return true;
	return false;
}

bool Node::isNextNodeMarked(int edgeIndex){
	if (edgeList.at(edgeIndex)->isNextNodeMarked())
		return true;
	return false;
}

bool Node::isAllNextNodeMarked(){
	for (int i = 0; i < edgeList.size(); i++){
		if (!edgeList.at(i)->isNextNodeMarked())
			return false;
	}
	return true;
}

Node *Node::getNextNode(int edgeIndex){
	return edgeList.at(edgeIndex)->getNextNode();
}

Node::~Node(){
	for (int i = 0; i < edgeList.size(); i++)
		delete edgeList.at(i);
}