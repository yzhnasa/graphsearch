#ifndef __Parser_H__
#define __Parser_H__
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "Node.h"
#include "Edge.h"
#include "Graph.h"
using namespace std;

class Parser{
private:
	static void splitString(string &lineBuffer, vector<string> &tokens);
public:
	static void parsingInputFile(string inputFileName, Graph &graph);
};
#endif