#include "Edge.h"

Edge::Edge(){
	this->nextNode = nullptr;
}

Edge::Edge(int prevNodeIndex, int nextNodeIndex){
	this->prevNodeIndex = prevNodeIndex;
	this->nextNodeIndex = nextNodeIndex;
}

Edge::Edge(int prevNodeIndex, int nextNodeIndex, Node *nextNode){
	this->prevNodeIndex = prevNodeIndex;
	this->nextNodeIndex = nextNodeIndex;
	this->nextNode = nextNode;
}

void Edge::setNextNode(Node *nextNode){
	this->nextNode = nextNode;
}

Node *Edge::getNextNode(){
	return nextNode;
}

void Edge::setNextNodeIndex(int nextNodeIndex){
	this->nextNodeIndex = nextNodeIndex;
}

int Edge::getNextNodeIndex(){
	return nextNodeIndex;
}

void Edge::setPrevNodeIndex(int prevNodeIndex){
	this->prevNodeIndex = prevNodeIndex;
}

int Edge::getPrevNodeIndex(){
	return prevNodeIndex;
}

bool Edge::isNextNodeMarked(){
	if (nextNode->isMarked())
		return true;
	return false;
}

Edge::~Edge(){

}