#include "Main.h"

int main(int argc, char *argv[]){
	if (argc != 2){
		cout << "Please input inputfile and outputfile name." << endl;
		exit(1);
	}
	Graph *graph = new Graph();
	Parser::parsingInputFile(argv[1], *graph);
	ostringstream outputBuffer;
	outputBuffer << "Breadth-First Search edge list:" << endl;
	graph->breadthFirstSearch();
	outputBuffer << graph->printEdgeOrder();
	outputBuffer << endl;
	outputBuffer << "Depth-First Search edge list:" << endl;
	graph->depthFirstSearch();
	outputBuffer << graph->printEdgeOrder();
	cout << outputBuffer.str();
	fstream outputStream("result_" + string(argv[1]), ios::out);
	outputStream << outputBuffer.str();
	delete graph;
	outputStream.close();
	return 0;
}