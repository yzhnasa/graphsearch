#ifndef __Graph_H__
#define __Graph_H__
#include <iostream>
#include <vector>
#include <queue>
#include <stack>
#include <fstream>
#include <sstream>
#include <string>
#include "Node.h"
#include "Edge.h"
using namespace std;
class Graph{
private:
	Node *startNode;
	Node *endNode;
	vector<Node *> nodeList;
	vector<Edge *> edgeOrderList;
	void initalGraph();
public:
	Graph();
	void putNode(Node *node);
	Node *getNode(int nodeIndex);
	void putEdgeToNode(int nodeIndex, Edge *edge);
	Edge *getNodeEdge(int nodeIndex, int edgeIndex);
	int graphSize();
	int nodeEdgeListSize(int nodeIndex);
	void breadthFirstSearch();
	void depthFirstSearch();
	string printEdgeOrder();
	~Graph();
};
#endif