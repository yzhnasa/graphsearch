#ifndef __Edge_H__
#define __Edge_H__
#include "Node.h"
using namespace std;
class Node;
class Edge{
private:
	Node *nextNode;
	int prevNodeIndex;
	int nextNodeIndex;
public:
	Edge();
	//Edge(Node *nextNode);
	Edge(int prevNodeIndex, int nextNodeIndex);
	Edge(int prevNodeIndex, int nextNodeIndex, Node *nextNode);
	void setNextNode(Node *nextNode);
	Node *getNextNode();
	void setNextNodeIndex(int nextNodeIndex);
	int getNextNodeIndex();
	void setPrevNodeIndex(int prevNodeIndex);
	int getPrevNodeIndex();
	bool isNextNodeMarked();
	~Edge();
};
#endif