#include "Graph.h"

Graph::Graph(){
	this->startNode = nullptr;
	this->endNode = nullptr;
}

void Graph::initalGraph(){
	for (int i = 0; i < nodeList.size(); i++){
		nodeList.at(i)->setUnmarked();
	}
	edgeOrderList.clear();
}

void Graph::putNode(Node *node){
	nodeList.push_back(node);
}

Node *Graph::getNode(int nodeIndex){
	return nodeList.at(nodeIndex);
}

void Graph::putEdgeToNode(int nodeIndex, Edge *edge){
	nodeList.at(nodeIndex)->putEdge(edge);
}

Edge *Graph::getNodeEdge(int nodeIndex, int edgeIndex){
	return nodeList.at(nodeIndex)->getEdge(edgeIndex);
}

int Graph::graphSize(){
	return nodeList.size();
}

int Graph::nodeEdgeListSize(int nodeIndex){
	return nodeList.at(nodeIndex)->edgeListSize();
}

void Graph::breadthFirstSearch(){
	initalGraph();
	queue<Node *> nodeQueue;
	nodeQueue.push(nodeList.at(0));
	nodeList.at(0)->setMarked();
	Node *tempNode = nullptr;
	while (!nodeQueue.empty()){
		tempNode = nodeQueue.front();
		if (!tempNode->isEdgeListEmpty()){
			int i = 0;
			while (i != tempNode->edgeListSize()){
				if (!tempNode->isNextNodeMarked(i)){
					nodeQueue.push(tempNode->getNextNode(i));
					tempNode->getNextNode(i)->setMarked();
					edgeOrderList.push_back(tempNode->getEdge(i));
				}
				i++;
			}
		}
		nodeQueue.pop();
	}
}

void Graph::depthFirstSearch(){
	initalGraph();
	stack<Node *> nodeStack;
	nodeStack.push(nodeList.at(0));
	nodeList.at(0)->setMarked();
	Node *tempNode = nullptr;
	while (!nodeStack.empty()){
		tempNode = nodeStack.top();
		if (!tempNode->isEdgeListEmpty() && !tempNode->isAllNextNodeMarked()){
			int i = 0;
			while (tempNode->isNextNodeMarked(i)){
				i++;
			}
			nodeStack.push(tempNode->getNextNode(i));
			tempNode->getNextNode(i)->setMarked();
			edgeOrderList.push_back(tempNode->getEdge(i));
		}
		else{
			nodeStack.pop();
		}
	}
}

string Graph::printEdgeOrder(){
	ostringstream outputBuffer;
	for (int i = 0; i < edgeOrderList.size(); i++){
		outputBuffer << "(";
		outputBuffer << edgeOrderList.at(i)->getPrevNodeIndex();
		outputBuffer << ", ";
		outputBuffer << edgeOrderList.at(i)->getNextNodeIndex();
		outputBuffer << ")" << endl;
	}
	cout << outputBuffer.str();
	return outputBuffer.str();
}

Graph::~Graph(){
	for (int i = 0; i < nodeList.size(); i++)
		delete nodeList.at(i);
}